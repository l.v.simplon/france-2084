import React from 'react';
import { Grid } from 'semantic-ui-react';
import MainMenuItem from "../components/MainMenuItem";

export default function PeopleItem({person}) {
    return <Grid stackable columns={3}>
        <Grid.Row>
            <h1>MENU</h1>
        </Grid.Row>
        <Grid.Column>
            <MainMenuItem to="/people" name="users" label="PEOPLE" />
        </Grid.Column>
        <Grid.Column>
            <MainMenuItem to="/people" name="spinner" label="PLACEHOLDER" loading disabled />
        </Grid.Column>
        <Grid.Column>
            <MainMenuItem to="/people" name="spinner" label="PLACEHOLDER" loading disabled />
        </Grid.Column>
    </Grid>
}