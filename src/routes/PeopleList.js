import React from 'react'
import { List } from 'semantic-ui-react'
import PeopleItem from '../components/PeopleItem'

export default function PeopleList({people}) {
    return <List size="massive" divided verticalAlign='middle'>
        {
            people.map((person) => <PeopleItem key={person._id} person={person} /> )
        }
    </List>
}