import data from "./data.json";
const sleep = async (ms) => new Promise(resolve => setTimeout(resolve, ms));

export async function getPeople(nb = 50) {
    await sleep(200);
    return {
        data: data.slice(0, nb)
    };
}

export async function searchPeople(key, value) {
    await sleep(200);
    const check = (obj) => obj[key] && obj[key] == value;

    const mainPeople = data.filter(check);
    const friends = data.map((person) => person.friends.filter(check)).flat();

    return {
        data: [...mainPeople, ...friends]
    };
}

export async function getUser(guid) {
    await sleep(200);
    const res = await searchPeople("guid", guid);
    const foundUsers = res.data;

    return {data: foundUsers ? foundUsers[0] : foundUsers};
}
